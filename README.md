Authors:

Jake & Osman

The project used tools including:

•	Java 11
•	Spring Tools 4
•	Spring Boot 2.4.5
•	Maven 3.6.3

The Objective of The Project

•	The objective of the project was to create two indepdendant SOAP Web Services
•	This was done using two xsd, two config, two repositories and two endpoint files

Testing 

•	SOAP UI was used to test both services

Packages

The project of consists of three Packages
1. A package to hold the config, repositories and endpoints
2. A package to hold generated source files for the student
3. A package to hold generated source files for the teacher
