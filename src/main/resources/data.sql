CREATE TABLE Student (
  
   	name varchar(255),
    standard int,
    address varchar(255)
    
);


insert into Student (name,standard,address) VALUES ('Osman Mohamed',1,'170-324 Red Deer');
insert into Student (name,standard,address) VALUES ('Jake',2,'280-324 Montreal');
insert into Student (name,standard,address) VALUES ('John Smith',3,'530-220 Saskatoon');

CREATE TABLE Teacher (
  
   	name varchar(255),
    experience int,
    course varchar(255)
    
);


insert into Teacher (name,experience,course) VALUES ('Doe',15,'Mathematics');
insert into Teacher (name,experience,course) VALUES ('Samira',20,'Sociology');
insert into Teacher (name,experience,course) VALUES ('Smith',30,'Political Studies');

