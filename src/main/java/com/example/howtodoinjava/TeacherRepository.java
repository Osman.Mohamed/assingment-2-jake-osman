package com.example.howtodoinjava;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.teacher.xml.school.Teacher;

@Component
public class TeacherRepository {
	
	private static final Map<String, Teacher> teachers = new HashMap<>();
	 
    @PostConstruct
    public void initData() {
         
        Teacher teacher = new Teacher();
        teacher.setName("John");
        teacher.setExperience(25);
        teacher.setCourse("Computer Science");
        teachers.put(teacher.getName(), teacher);
        
        teacher = new Teacher();
        teacher.setName("Williams");
        teacher.setExperience(17);
        teacher.setCourse("Sociology");
        teachers.put(teacher.getName(), teacher);
        
        teacher = new Teacher();
        teacher.setName("Serah");
        teacher.setExperience(12);
        teacher.setCourse("Biology");
        teachers.put(teacher.getName(), teacher);
        
        teacher = new Teacher();
        teacher.setName("David");
        teacher.setExperience(23);
        teacher.setCourse("Mathematics");
        teachers.put(teacher.getName(), teacher);
        
    }
 
    public Teacher findTeacher(String name) {
        Assert.notNull(name, "The Teacher's name must not be null");
        return teachers.get(name);
    }

}
