package com.example.howtodoinjava;


import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
 
@EnableWs
@Configuration
public class TeacherConfig extends WsConfigurerAdapter 
{
    @Bean
    public ServletRegistrationBean teacherDispatcherServlet(ApplicationContext applicationContext) 
    {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/service/*");
    }
 
    @Bean(name = "teacher")
    public DefaultWsdl11Definition teacherWsdl11Definition(XsdSchema teachersSchema) 
    {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("TeacherDetailsPort");
        wsdl11Definition.setLocationUri("/service/teacher-details");
        wsdl11Definition.setTargetNamespace("http://www.teacher.com/xml/school");
        wsdl11Definition.setSchema(teachersSchema);
        return wsdl11Definition;
    }
 
    @Bean
    public XsdSchema teachersSchema() 
    {
        return new SimpleXsdSchema(new ClassPathResource("teacher.xsd"));
    }
}