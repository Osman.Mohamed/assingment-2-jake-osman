package com.example.howtodoinjava;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.howtodoinjava.xml.school.StudentDetailsRequest;
import com.howtodoinjava.xml.school.StudentDetailsResponse;
import com.teacher.xml.school.TeacherDetailsRequest;
import com.teacher.xml.school.TeacherDetailsResponse;
 
@Endpoint
public class TeacherEndpoint {
    private static final String NAMESPACE_URI = "http://www.teacher.com/xml/school";
 
    private TeacherRepository TeacherRepository;
    
 
    @Autowired
    public TeacherEndpoint(TeacherRepository TeacherRepository) {
        this.TeacherRepository = TeacherRepository;
    }
   
 
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TeacherDetailsRequest")
    @ResponsePayload
    public TeacherDetailsResponse getTeacher(@RequestPayload TeacherDetailsRequest request) {
        TeacherDetailsResponse response = new TeacherDetailsResponse();
        response.setTeacher(TeacherRepository.findTeacher(request.getName()));
 
        return response;
    }
    
    
}